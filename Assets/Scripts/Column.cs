﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour
{
    Rigidbody2D rigidBody;
    GameManager manager;

    // Start is called before the first frame update
    void Start()
    {

    }
    public void Init(GameManager manager)
    {
        rigidBody = gameObject.AddComponent<Rigidbody2D>() as Rigidbody2D;
        rigidBody.bodyType = RigidbodyType2D.Kinematic;
        manager.UpdateColumn += delegate { UpdateSpeed(manager.ScrollSpeed); };
    }

    public void UpdateSpeed(float speed)
    {
        rigidBody.velocity = new Vector2(-speed, 0);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
