﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public delegate void UpdateColumnDelegate();
    public Text ScoresText;
    public event UpdateColumnDelegate UpdateColumn;
    public float ScrollSpeed = 10;
    public int Scores = 0;

    void Start()
    {
        UpdateColumn += delegate { };
    }

    public void NextColumn()
    {
        Scores++;
        ScrollSpeed += Mathf.Sqrt(ScrollSpeed/100);
        UpdateColumn();
        ScoresText.text = "Scores:" + Scores;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
