﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bird : MonoBehaviour
{

    private Animator animator; 
    private Rigidbody2D rigidBody;

    public float flapforce;
    GameObject render;
    bool isLife { get; set; } 

    // Start is called before the first frame update
    void Start()
    {
        isLife = true;
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
        render = Instantiate(Resources.Load<GameObject>("Birds/MainBird"), transform);
    }

    public void Flap()
    {
        if (isLife)
        {
            animator.SetTrigger("Flap");
            rigidBody.velocity = (new Vector2(0, flapforce));
        }
    }


    public void Die()
    {
        rigidBody.velocity = Vector2.zero;
        animator.SetTrigger("Die");
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        isLife = false;
            Die();
            Invoke("endGame", 2);
    }
    void endGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
