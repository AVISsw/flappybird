﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public GameObject columnPrefab;
    public float columnMinHeigth = -3f;
    public float columnMaxHeigth = 3f;
    public float spawnRate = 3f;
    public int columnCount = 10;
    public Transform transformSpawnColumn;

    public GameManager manager;

    private int currentColumn = 0;
    private float spawnOffsetX = 15f;
    private float lastSpawn = 0f;
    private Column[] columns;

    void Start()
    {
        columns = new Column[columnCount];
        for (int i = 0; i < columnCount; i++)
        {
           var obj = (GameObject)Instantiate(columnPrefab, transformSpawnColumn.position, Quaternion.identity);
           var column = obj.AddComponent<Column>();
            column.Init(manager);
            columns[i] = column;
        }
    }

    // Update is called once per frame
    void Update()
    {
        lastSpawn += Time.deltaTime;

        if (lastSpawn >= spawnRate)
        {
            lastSpawn = 0f;
            float spawnOffsetY =  Random.Range(columnMinHeigth, columnMaxHeigth);
            columns[currentColumn].transform.position = new Vector2(spawnOffsetX, spawnOffsetY);
            
            currentColumn++;
            manager.NextColumn();

            if (currentColumn >= columnCount)
            {
                currentColumn = 0;
            }
        }
    }
}
